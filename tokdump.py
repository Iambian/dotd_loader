''' Uses usernames and passwords to generate direct login links from NG for DotD
    Usage: python tokdump.py [portal_page_to_patch]
    
    Without argument, returns javascript array of direct login links.
    With argument being the portal page, it patches it with direct links.

'''
import os,sys
sys.path.append(os.path.abspath('.'))
import re,httplib,urllib,time,Cookie,zlib,ssl,datetime,json
from collections import OrderedDict
import accounts

try:
    fp = open("cookiecache.json","r")
    cookiecache = json.load(fp)
except:
    cookiecache = {}
    
USE_COOKIECACHE = True
DEFAULT_CONNTYPE = "NG"  #valid values: NG,AG,FB,K,SA

## ---------------------------------------------------------
POSTHEADER = {"content-type":"application/x-www-form-urlencoded","accept":"text/html"}


def gethval(header,name):
    for i in header:
        if (i[0].lower() == name.lower()): return i[1]
    return ''
def getcookie(header):
    return gethval(header,"set-cookie")
def ununicode(data):
    if isinstance(data,dict):
        return dict([(ununicode(k),ununicode(v)) for k,v in data.iteritems()])
    elif isinstance(data,list):
        return [ununicode(v) for v in data]
    elif isinstance(data,unicode):
        return data.encode("utf-8")
    else:
        return data
    
    
cookiecache = ununicode(cookiecache)
class Acct(object):
    accts = []
    maxattempts = 10
    def __init__(self,username,password,conntype=None):
        global DEFAULT_CONNTYPE,cookiecache,USE_COOKIECACHE
        if conntype:    self.site = conntype
        else:           self.site = DEFAULT_CONNTYPE
        self.username = username
        self.password = password
        self.dirlink = None
        Acct.accts.append(self)
        self.attemptsremain = Acct.maxattempts
        self.cookiecache = None
        try:
            self.cookiecache = cookiecache[conntype+username]
        except:
            pass
        pass
        
    @classmethod
    def resetall(cls):
        for a in cls.accts:
            a.attemptsremain = Acct.maxattempts
            a.dirlink = None
            
    @classmethod
    def getlinks(cls):
        is_done = False
        while not is_done:
            ## At the start of each loop, check if any accts needs to be fetched
            for a in cls.accts:
                if not a.dirlink and a.attemptsremain > 0:
                    break
            else:
                ##If complete without brak, all accts filled in. Stop trying.
                is_done = True
                break
            ## Begin fetching links for all accounts
            for a in cls.accts:
                a.getlink()
    @classmethod
    def getjs(cls):
        sdat = "var GAMEURLDATABASE = [\n"
        for a in cls.accts:
            if a.dirlink:
                sdat += '    ["'+str(a.username)+'","'+str(a.dirlink)+'"],\n'
        sdat += "];\n"
        return sdat
        
    @classmethod
    def dumpcookies(cls):
        global cookiecache
        for a in cls.accts:
            try:
                if cc != a.cookiecache:
                    cookiecache[a.site+a.username] = a.cookiecache
            except:
                cookiecache[a.site+a.username] = a.cookiecache
        return cookiecache
       
    def getlink(self):
        for i in range(self.attemptsremain):
            try:
                link = self._getlinkbase()
            except:
                link = None
            if not link:
                if link is not None:
                    self.cookiecache = ''  # Clear cache if non-timeout error
                if self.attemptsremain:
                    s = "Retrying in 5 seconds."
                else:
                    s = "Skipping this account."
                print "Link get failed for "+str(self.username)+" "+s
                if not self.attemptsremain:
                    return ''
                time.sleep(5)
            else:
                print "Link retrieved for "+str(self.username)+" on platform "+str(self.site)
                self.dirlink = link
                return link
        
    def request(self,method="GET",target='/',body=None,headers={}):
        if   self.site == "NG": host = "www.newgrounds.com"
        elif self.site == "AG": host = "www.armorgames.com"
        elif self.site == "FB": host = "www.facebook.com"
        elif self.site == "K":  host = "www.kongregate.com"
        elif self.site == "SA": host = "www.dawnofthedragons.com"
        conn = httplib.HTTPSConnection(host)
        headers['accept-encoding'] = "identity,gzip"
        conn.request(method,target,body,headers)
        resp = conn.getresponse()
        head = resp.getheaders()
        body = resp.read()
        conn.close()
        if (gethval(head,"content-encoding") == "gzip"):
            body = zlib.decompress(body,32+zlib.MAX_WBITS)
        time.sleep(0.5)
        return (resp.status,body,head)
        
    def _returnlink(self,body):
        start  = body.find('"https://dotd-web1.')
        end    = body.find('"',start+2)
        if (start == -1 or end == -1): return ''
        string = body[start+1:end]
        return string
    
    def _getlinkbase(self):
        global USE_COOKIECACHE,cookiecache
        usecache = USE_COOKIECACHE
        if self.site == "NG":
            if not (usecache and self.cookiecache):
                ''' FIND LOGIN PORTAL TOKEN '''
                (s,b1,h1) = self.request("GET","/passport/")
                start = b1.find('"/passport')
                end   = b1.find('"',start+1)
                if (start<0 or end<0):
                    return ''
                login_target = b1[start+1:end]
                print "Retrieved login page"
                ''' SEND LOGIN REQUEST AND (HOPEFULLY) RECEIVE LOGIN COOKIE '''
                params = urllib.urlencode( {
                    "username":self.username,"password":self.password,
                    "remember":1,"login":1
                })
                (s,b2,h2) = self.request("POST",login_target,params,POSTHEADER)
                self.cookiecache = h2
            else:
                print "Using cookie cache"
                h2 = self.cookiecache
            ''' GET GAME PAGE USING CREDENTIALS AND SCAN FOR DIRECT LINK '''
            cookie = Cookie.SimpleCookie(getcookie(h2)).output("","",";")
            header = {"Cookie": cookie}
            (s,b3,h3) = self.request("GET","/portal/view/609826",None,header)
            
            ''' FIND URL IN PAGE '''
            start = b3.find('<iframe src=\\"https:')
            end   = b3.find('"',start+18)
            if (start == -1 or end == -1):
                return ''
            string = b3[start+14:end-1]
            string = string.replace("\\","")
            return string
        if self.site == "AG":
            if not (usecache and self.cookiecache):
                ''' SEND LOGIN REQUEST AND (HOPEFULLY) RECEIVE LOGIN COOKIE '''
                params = urllib.urlencode({ 
                    "username":self.username,"password":self.password,
                    "act":"login","referer":"https://armorgames.com"
                })
                (s,b2,h2) = self.request("POST","/login/",params,POSTHEADER)
                self.cookiecache = h2
            else:
                h2 = self.cookiecache
            ''' GET GAME PAGE USING CREDENTIALS AND SCAN FOR DIRECT LINK '''
            cookie = Cookie.SimpleCookie(getcookie(h2)).output("","",";")
            header = {"Cookie": cookie}
            (s,b3,h3) = self.request("GET","/dawn-of-the-dragons-game/13509",None,header)
            
            ''' FIND URL IN PAGE '''
            return self._returnlink(b3)
        

##
## PROGRAM START
##

if __name__ == '__main__': 
    
    for i in accounts.ACCOUNTS:
        Acct(i[1],i[2],i[0])
    
    Acct.getlinks()
    Acct.dumpcookies()
    sdata = Acct.getjs()

    patchfile = ""
    if (len(sys.argv)>1):
        patchfile = sys.argv[1]
    else:
        patchfile = "portal.html"
    try:
        htdata = open(patchfile,"r").read()
        start = htdata.find("var GAMEURLDATABASE")
        end   = htdata.find("];",start);
        htdata = htdata[:start] + sdata + htdata[end+3:]
        open(patchfile,"w").write(htdata)
        print "Web portal link database patched."
    except:
        patchfile = ""
        print "Failed to patch portal link database"

    if not patchfile:
        print "Writing to RESULTS.txt"
        open("RESULTS.txt",'w').write(sdata)
    print "Writing cached cookie data to cookiecache.json"
    try:
        fp = open("cookiecache.json","w")
        json.dump(cookiecache,fp)
    except:
        print "Cookie data write failure."

    print "Writing complete."

