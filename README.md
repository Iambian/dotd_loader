Dawn of the Dragons Standalone Game Page Loader/Changer
=======================================================
Warning:
* The link fetch tool (`tokdump.py`) will only work for accounts registered on Newgrounds.


Motivation
----------
Messing around with alternate accounts is such a messy business. For each
account, one has to log out of a portal site (such as Newgrounds or Armor Games)
, then log back in as another person to change accounts. These portal sites also
sometimes serves up ads, either as a prerequisite to playing the game itself, or
somewhere else on the page, causing unwanted memory and CPU usage.

I wrote this utility to help bypass the need for cycling logins and the bloat
that playing on a portal website may cause, which makes getting to the game and
playing it faster and less resource-intensive.

Requirements
------------
* Python 2.7.x. Make sure you select to set up the path component when installing.
  This is not selected by default.

How It Works
------------
The `portal.html` page that you open in your browser keeps all the direct links
to the 5PG (Dawn of the Dragons) site. Platforms, such as Newgrounds or
Kongregate, will generate these links to contain session data to verify to 5PG
that you are who you say you are. The portal page's source is expected to be
modified to include these links in its database so you can easily switch between
them.

The `tokdump.py` script regenerates and optionally allows you to patch `portal.html`
for you by using the username and password pairs that you edit into the script
to fetch the links for you. Such was required for Newgrounds as the login links
expire after six (6) hours of inactivity. Which sucks if you're packing many
alternate accounts.

How to Use
----------
To **find the game links that you will need manually**, log into the game the
way you would normally. After it is logged in, open the page in your browser's
Element Inspector and find (Ctrl+F) the phrase `https://dotd-web1.5thplanetgames.com/` .

![](img/example3.PNG)

The rest of the link that is there will be the game link to copy. **DO NOT SHARE
THIS LINK SINCE IT CAN BE USED BY ANYONE TO PLAY YOUR GAME WHETHER OR NOT THEY
ARE LOGGED IN**

---

To **enter your game links into the portal's database**, open `portal.html` up in
a text editor, such as Notepad.exe, or Notepad++. Find the line that starts with 

`var GAMEURLDATABASE = `

and insert the links where approrpiate, adding new lines as needed. It may look
something like this:

![](img/example2.png)

If you're having trouble figuring it out, read up on 
[Javascript arrays here](https://www.w3schools.com/js/js_arrays.asp)
to gain a better understanding of what it is you're changing.

---

Open `portal.html` in your browser. It may look something like this, except much crisper.
**Annotations are in red**

![](img/example4.PNG)

* IE users: Go use another browser. My portal page will not work in it.
* Firefox users: [Enable local execution of Flash](https://support.mozilla.org/en-US/questions/1172126)
  then enable Flash to run in general from the settings menu. Do this even if Flash
  runs on other sites.
* Chrome users: Go to the options menu and ensure that Flash will run. Do this even
  if Flash runs on other sites.
* Opera users: idk. not tested.
* whatever browser is on Mac: I don't own a Mac. Can't test. Don't ask me.
* Other browsers: Follow your browser's steps to enable flash, both in general and
  locally (if run from your computer instead of from the internet).
  
---

Using the Game Link Retrieval Tool
----------------------------------

* **THIS TOOL ONLY WORKS WITH NEWGROUNDS**

**THIS TOOL WANTS YOUR NEWGROUNDS LOGIN INFORMATION. PROTECT YOUR LOGIN INFORMATION.
CAREFULLY REVIEW THE SCRIPT TO MAKE SURE THAT THE LOGIN INFORMATION DOES NOT GO
ANYWHERE YOU DO NOT WANT IT TO GO. IF YOU DO NOT FEEL COMFORTABLE WITH THIS, DO
NOT USE THIS SCRIPT.**

Modify `tokdump.py` to include all your alts' login information. An example:

![](img/example5.PNG)

Save this script, then open a command-line prompt, invoking Python:

`python tokdump.py portal.html`

If it runs successfully, `portal.html` should be patched with the links that it
retrieves, making it unnecessary to edit the portal page.

  
Troubleshooting
---------------
* **The drop-down menu is empty**
	* Make sure you saved `portal.html` before opening it.
	* Hit Ctrl+F5 after loading it to force a cache clear. 
	* Make sure that the array the links are in are valid.
	(see your browser's javascript console to see if there are errors). 
	* Is your browser's javascript enabled? 
	* Are you using Internet Explorer? (Don't.)
* **Nothing appears to be loading in the game area**
	* Follow all the steps as from before.
	* Are the game links valid? 
	  (pasting them as-is into the address bar of a new window should work to check)
* **The game loads but refuses to log in**
	* Use the built-in Refresh button and try again
	* If you're on NG, log in and get your link again. They expire after 6 hours of inactivity.

  

Known Problems
--------------

* The drop-down menu's contents in `portal.html` has to change value before it can
  be acccepted. Selecting the same option that was already there is not changing it.
  Workaround: Select `::select acct::` and then select the option you really wanted.
* Something doesn't work right in Internet Explorer. I forgot exactly what it was
  but the game was literally unplayable for me when I tested it. I refuse to test
  it again in that browser.

License
-------
MIT. See LICENSE

Version History
---------------
0.1 - Initial commit
0.2 - Changed default behavior to always patch portal.html, letting users simply
      double-click the python file

